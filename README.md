![Breakout Demo](https://i.imgur.com/MktWSAU.mp4)

UNITY 2018.1
This is a basic unity template for a breakout type game.

Includes paddle movement and ball movement.

Includes the tricky part where some get stuck by which i mean the ball heads more to the left if you hit with the left side of the paddle and vice versa.

includes some basic art i made in GIMP.

You are free to use this project to start your own, but do not publish as is. 

Use WASD to move the paddle and that's it.

You can play the WebGL demo [Here](https://crumble.gitlab.io/breakout)